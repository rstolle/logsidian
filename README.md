# logseq vs obsidian

[logseq](https://logseq.com) and [obsidian](https://obsidian.md) are powerful tools for taking notes and connected them
to each other. Both tools have many things in common:

- Notes are stored in a **local folder**. It's possible to sync them on a remote server, but you are not forced to do
  so.
- [Markdown](https://en.wikipedia.org/wiki/Markdown) is used to write down notes.
- You can include other files like pictures, videos, audios, pdf and more

There are awesome features like timestamping youtube videos, marking text in pdf files, manage TOODs and much, much
more. Both tools have a plugin interface which allows endless possibilities.

## Markdown as the basic file format

Both tools extend the markdown syntax with a few features:

```txt
[[another note]] - a direct link to the note in the file 'another note.md'

#ONGOING  - a clickable tag

$\sqrt{3x + 5}$ - math formulas
```

The files look similar but there are [differences](doc/differences.md) which makes it hard to switch between the
both tools. I want to point out those differences and offer tools to convert existing notes be used in the other tool,
especially converting a logseq graph to an obsidian vault. There is
[such a tool available](https://github.com/NishantTharani/LogSeqToObsidian) already, and it works somehow. But it had
some quirks I want to overcome. Since I'm not a python guy, I started this project.