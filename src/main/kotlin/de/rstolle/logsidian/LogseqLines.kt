package de.rstolle.logsidian

import de.rstolle.logsidian.LogseqLineType.*
import java.nio.file.Path
import kotlin.io.path.readLines

enum class LogseqLineType { PROPERTY, CONTENT, ALL }

internal class LogseqLines(path: Path) {

    private val _propertyLines = mutableListOf<String>()
    private val _contentLines = mutableListOf<String>()

    val properties: List<LogseqProperty> = buildList {
        var inHead = true
        path.readLines().forEachIndexed { idx, line ->
            val prop = LogseqProperty.orNull(line)

            if (inHead) {
                if (prop == null) {
                    inHead = false
                    _contentLines.add(line)
                } else {
                    add(prop)
                    _propertyLines.add(line)
                }
            } else {
                if (prop != null && prop.name != "id") {
                    throw IllegalArgumentException(
                        "non id:: Property within content: $prop\n" +
                                "$path:${idx + 1} -> $line"
                    )
                }
                _contentLines.add(line)
            }
        }
    }

    private val contentLines = _contentLines.toList()
    private val propertyLines = _propertyLines.toList()
    private val totalLines = propertyLines + contentLines

    fun lines(type: LogseqLineType = ALL) = when (type) {
        PROPERTY -> propertyLines
        CONTENT -> contentLines
        ALL -> totalLines
    }
}