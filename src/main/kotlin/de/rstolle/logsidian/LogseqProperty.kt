package de.rstolle.logsidian

import io.github.aakira.napier.Napier

data class LogseqProperty(val name: String, val values: List<String>) {

    companion object {

        const val TAGS = "tags"
        const val ALIAS = "alias"

        val DEFAULT_PROPERTY_NAME_REGEX = "^[a-z][a-z-_0-9]*$".toRegex()

        fun isValidPropertyLine(line: String, namingRules: Regex = DEFAULT_PROPERTY_NAME_REGEX) =
            orNull(line, namingRules) != null

        fun orNull(line: String, namingRules: Regex = DEFAULT_PROPERTY_NAME_REGEX): LogseqProperty? {
            return try {
                parse(line, namingRules)
            } catch (fail: IllegalArgumentException) {
                Napier.d { "silently failed parsing property line: '$line'" }
                return null
            }
        }

        fun parse(line: String, namingRules: Regex = DEFAULT_PROPERTY_NAME_REGEX): LogseqProperty {
            require(line.contains(":: ")) { "property line not containing ':: ' '$line'" }
            require(line.length > 4) { "property line too short: '$line'" }

            val splits = line.split(":: ")
            require(splits.size == 2) { "property line not correctly splittable: '$line'" }

            val name = splits.first()

            require(namingRules.matches(name)) { "property name does not follow ${namingRules.pattern}: '$line'" }
            require(name.isNotBlank()) { "property with blank name: '$line'" }

            val properties = buildList<String> {
                splits[1].split(",").forEach() {
                    // shall I require(value.isNotBlank()) { "blank property value: " }
                    add(it.trim())
                }
            }
            return LogseqProperty(name, properties)
        }
    }
}