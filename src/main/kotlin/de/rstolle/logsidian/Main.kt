package de.rstolle.logsidian

fun main(args: Array<String>) {
    if (args.size == 1) {
        val parser = LogseqGraph(args[0])
        parser.printStats()
    } else {
        println("usage: prg path_to_logseq_graph")
    }
}
