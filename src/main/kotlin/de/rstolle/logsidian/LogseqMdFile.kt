package de.rstolle.logsidian

import de.rstolle.logsidian.LogseqLineType.ALL
import de.rstolle.logsidian.LogseqLineType.CONTENT
import de.rstolle.tools.requireReadableFile
import java.nio.file.Path
import kotlin.io.path.extension

class LogseqMdFile(path: Path) {
    val path = path.requireReadableFile()

    init {
        require(path.extension == "md") { "non md extension Page: $path" }
    }

    val name = path.last().toString().dropLast(3)

    private val _lines = LogseqLines(path)

    val properties = _lines.properties

    val propertyNames = buildList {
        properties.forEach {
            add(it.name)
        }
    }

    fun containsProperty(name: String) = propertyNames.contains(name)

    fun propertyOrNull(name: String): LogseqProperty? {
        properties.forEach {
            if (it.name == name) {
                return it
            }
        }
        return null
    }

    /**
     * Retrieves ALL links from this File, including links in the page properties.
     * ** In page tag-Property, all values will be treated as links. If not marked so, they will be
     * [LogseqLinkType.EXPLICIT]**
     */
    fun links(type: LogseqLineType = ALL) = buildList {
        if (type == LogseqLineType.PROPERTY || type == ALL) {
            properties.forEach {
                addAll(LogseqLink.fromProperty(it))
            }
        }
        if (type == CONTENT || type == ALL) {
            lines(CONTENT).forEach { addAll(LogseqLink.fromLine(it)) }
        }
    }

    override fun toString(): String {
        return "LogseqMdFile: $path; ${_lines.lines().count()} lines"
    }

    fun lines(type: LogseqLineType = ALL) = _lines.lines(type)
}


