package de.rstolle.logsidian

import de.rstolle.logsidian.LogseqLineType.ALL
import java.nio.file.Path
import kotlin.io.path.forEachDirectoryEntry

class LogseqMdFolder(val folder: Path) {

    val mdfiles = buildList {
        folder.forEachDirectoryEntry {
            add(LogseqMdFile(it))
        }
    }.sortedBy { it.name }

    fun lines(type: LogseqLineType = ALL) = buildList {
        mdfiles.forEach {
            addAll(it.lines(type))
        }
    }

    fun links(type: LogseqLineType = ALL): List<LogseqLink> =
        buildList {
            for (file in mdfiles) {
                addAll(file.links(type))
            }
        }

    fun grep(expression: String, type: LogseqLineType = ALL): Map<String, String> {
        return buildMap {
            mdfiles.forEach { file ->
                file.lines(type).forEachIndexed { idx, line ->
                    if (line.contains(expression)) {
                        put("${file.name}:${idx + 1}", line)
                    }
                }
            }
        }
    }

    fun getPageOrNull(name: String): LogseqMdFile? {
        val list = mdfiles.filter { it.name.uppercase() == name.uppercase() }
        if (list.isEmpty()) {
            return null
        }
        if (list.size > 1) {
            throw IllegalStateException("page '$name' exists ${list.size} times")
        }
        return list.first()
    }
}