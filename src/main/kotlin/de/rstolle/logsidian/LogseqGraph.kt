package de.rstolle.logsidian

import de.rstolle.logsidian.LogseqLineType.*
import de.rstolle.tools.requireReadableFile
import de.rstolle.tools.requireReadableFolder
import java.nio.file.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.exists
import kotlin.io.path.forEachDirectoryEntry
import kotlin.io.path.writeText

const val DEFAULT_FOLDER_PAGES = "pages"
const val DEFAULT_FOLDER_JOURNALS = "journals"
const val DEFAULT_FOLDER_ASSETS = "assets"
const val EMPTY_LINE = "\n"

class LogseqGraph(
    rootFolder: Path,
    pagesFolderName: String = DEFAULT_FOLDER_PAGES,
    journalsFolderName: String = DEFAULT_FOLDER_JOURNALS,
    assetsFolderName: String = DEFAULT_FOLDER_ASSETS
) {
    constructor(rootFolder: String) : this(Path.of(rootFolder))

    val rootFolder = rootFolder.requireReadableFolder("logseq folder")

    val pagesFolder = rootFolder.resolve(pagesFolderName).requireReadableFolder("pages folder")
    val journalsFolder: Path = rootFolder.resolve(journalsFolderName).requireReadableFolder("journals folder")
    val assetsFolder: Path? = if (rootFolder.resolve(assetsFolderName).exists())
        rootFolder.resolve(assetsFolderName).requireReadableFolder("assets folder")
    else
        null

    val pages = LogseqMdFolder(pagesFolder)
    val journals = LogseqMdFolder(journalsFolder)
    val mdfiles = pages.mdfiles + journals.mdfiles


    val assets = buildList {
        assetsFolder?.let {
            assetsFolder.forEachDirectoryEntry {
                it.requireReadableFile("Asset File")
                add(it)
            }
        }
    }

    fun grep(expression: String, type: LogseqLineType = ALL) =
        journals.grep(expression, type) + pages.grep(expression, type)

    fun lines(type: LogseqLineType = ALL) = journals.lines(type) + pages.lines(type)

    fun links(type: LogseqLineType = ALL) = journals.links(type) + pages.links(type)

    fun printStats() {
        println("----------------------------------------------------")
        println("logseq graph in: $rootFolder")
        println("          pages: $pagesFolder")
        println("       journals: $journalsFolder")
        println("         assets: $assetsFolder")
        println("----------------------------------------------------")
        println(" pages     : ${pages.mdfiles.size}")
        println(" journals  : ${journals.mdfiles.size}")
        println(" lines     : ${lines()}\n")
        println(" assets    : ${assets.size}")
        println("----------------------------------------------------")

        println("Links total:    ${links().size}")

        println("Links distinct: ${links().distinct().size}")
        println("Links distinct: ${links(PROPERTY).distinct().size}")
        println("Links distinct: ${links(CONTENT).distinct().size}")
    }

    fun findPageTags(type: LogseqLinkType = LogseqLinkType.NONE): List<LogseqLink> {
        return buildList {
            mdfiles.forEach { page ->
                page.properties.filter { it.name == LogseqProperty.TAGS }.forEach { tags ->
                    val links = LogseqLink.fromProperty(tags)
                    addAll(links.filter { type == LogseqLinkType.NONE || type == it.type })
                }
            }
        }
    }

    /**
     * @return the [LogseqMdFile] [link] points to or null if there is no such page
     */
    fun getFileOrNull(link: LogseqLink): LogseqMdFile? {
        val linked = pages.mdfiles.filter { link.isLinkedTo(it) }
        if (linked.size == 0) {
            return null
        }

        if (linked.size > 1) {
            throw IllegalStateException("$link points to more than one page: $linked")
        }

        return linked.first()
    }

    /**
     * A pure Tag (for now) is a link with no linked page.
     */
    fun isPureTag(link: LogseqLink) = getFileOrNull(link) == null


    /**
     * Finds all the page tags which are not linked to any page.
     */
    fun findPageTagsPure(type: LogseqLinkType = LogseqLinkType.NONE) =
        findPageTags(type).filter { isPureTag(it) }

    /**
     * Finds all the page tags which have a [LogseqMdFile] linked to it.
     */
    fun findPageTagsLinked(type: LogseqLinkType = LogseqLinkType.NONE) =
        findPageTags(type).filter { !isPureTag(it) }

    fun getPageOrNull(name: String): LogseqMdFile? {
        return pages.getPageOrNull(name)
    }

    fun getJournalOrNull(name: String): LogseqMdFile? {
        return journals.getPageOrNull(name)
    }

    fun obsideProperties(page: LogseqMdFile): String =
        if (page.properties.isEmpty()) {
            ""
        } else {
            buildString {
                append("---\n")
                page.properties.forEach {
                    append(obsideProperty(it))
                }
                append("---\n")
            }
        }

    fun obsideProperty(prop: LogseqProperty) =
        when (prop.name) {
            LogseqProperty.TAGS -> taglinkLists(LogseqLink.fromProperty(prop))
            LogseqProperty.ALIAS -> createYamlList("aliases", prop.values)
            else -> createYaml(prop)
        }

    private fun createYaml(prop: LogseqProperty) =
        if (prop.values.size == 1) {
            createYamlLine(prop)
        } else {
            createYamlList(prop)
        }


    private fun taglinkLists(mixed: List<LogseqLink>): String {

        val tags = mixed.filter { isPureTag(it) }
        val links = mixed.filter { !isPureTag(it) }

        return buildString {
            if (tags.isNotEmpty()) {
                append(createYamlTags(tags))
            }

            if (links.isNotEmpty()) {
                append(createYamlLinks(links))
            }
        }
    }

    private fun createYamlTags(tags: List<LogseqLink>): String {
        return buildString {
            append("tags:\n")
            tags.forEach {
                append("  - ${it.obsideTagName()}\n")
            }
        }
    }

    private fun createYamlLinks(tags: List<LogseqLink>): String {
        return buildString {
            append("links:\n")
            tags.forEach {
                append("  - \"[[${it.name}]]\"\n")
            }
        }
    }


    private fun createYamlLine(prop: LogseqProperty): String {
        return "${prop.name}: ${obsPropertyLink(prop.values.first())}\n"
    }


    private fun createYamlList(prop: LogseqProperty) = createYamlList(prop.name, prop.values)

    private fun createYamlList(name: String, values: List<String>): String {
        return buildString {
            append("${name}:\n")
            values.forEach {
                append("  - ${obsPropertyLink(it)}\n")
            }
        }
    }

    private fun obsPropertyLink(raw: String): String {
        if (LogseqLink.isLink(raw)) {
            val link = LogseqLink(raw)
            return "\"[[${link.name}]]\""
        }
        return raw
    }


    fun obsideLine(line: String): String {
        if (line.isBlank()) {
            return EMPTY_LINE
        }

        val mixed = LogseqLink.fromLine(line)
        if (mixed.isEmpty()) {
            return line
        }
        var newLine = line
        mixed.forEach { link ->
            if (isPureTag(link)) {
                newLine = newLine.replace(link.raw, "#${link.obsideTagName()}")
            } else {
                newLine = newLine.replace(link.raw, "[[${link.name}]]")
            }
        }
        return newLine
    }

    fun obsideContent(page: LogseqMdFile) =
        buildString {
            page.lines(CONTENT).forEach {
                append(obsideLine(it))
            }
        }

    fun obsidePage(page: LogseqMdFile) =
        buildString {
            append(obsideProperties(page))
            append(EMPTY_LINE)
            append(obsideContent(page))
        }

    fun obside(destination: Path) {
        if (destination.exists()) {
            if (destination.any()) {
                throw IllegalArgumentException("$destination is not empty")
            }
        } else {
            destination.createDirectory()
        }

        pages.mdfiles.forEach { page ->
            val dest = destination.resolve("${page.name}.md")
            println("writing $dest")
            dest.writeText(obsidePage(page))
        }
    }
}


