package de.rstolle.logsidian


val REGEX_WHITES = "\\s".toRegex()
val REGEX_WIKI_LINK_AT_LINE_START = "^\\[\\[(.*?)]]".toRegex()
val REMOVE_ON_OBSIDIAN_TAGS = "[^a-zA-Z0-9-]".toRegex()

/**
 * Represents a Logseq link. There is a concrete [LogseqLinkType] for each link, which doesn't matter too much, cause
 * Logseq makes no difference between these type. The real important part is the [name] which represents the target
 * of the link
 *
 * @property raw The raw string representation of the link.
 * @throws IllegalArgumentException if [raw] is not a valid link
 */
class LogseqLink(val raw: String, explicit: Boolean = false) {

    val type = let {
        val detected = LogseqLinkType.detect(raw)
        if (detected == LogseqLinkType.NONE) {
            if (explicit) {
                LogseqLinkType.EXPLICIT
            } else {
                throw IllegalArgumentException("Could not detect link type for '$raw'")
            }
        } else {
            detected
        }
    }

    val name = type.extractName(raw)

    fun isLinkedTo(page: LogseqMdFile) = name.uppercase() == page.name.uppercase()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LogseqLink) return false
        return name.uppercase() == other.name.uppercase()
    }

    override fun hashCode(): Int {
        return raw.hashCode() + name.hashCode()
    }

    override fun toString(): String {
        return "$type-Link to '$name'"
    }

    fun obsideTagName(): String {
        return obsideTagName(name)
    }

    companion object {

        /**
         * Gets all the links from a text.
         */
        fun fromText(lines: String): List<LogseqLink> {
            return buildList {
                lines.lines().forEach { line ->
                    addAll(fromLine(line))
                }
            }
        }

        /**
         * Gets all the links from **one** line.
         */
        fun fromLine(line: String): List<LogseqLink> {
            return buildList {
                extractRawLinks(line).forEach {
                    add(LogseqLink(it))
                }
            }
        }


        fun isLink(raw: String): Boolean = LogseqLinkType.detect(raw) != LogseqLinkType.NONE

        /**
         * Returns ALL values of [property] which are links. If the property name is [LogseqProperty.TAGS], ALL
         * values will be treated as [LogseqLink] - The one not marked so will be [LogseqLinkType.EXPLICIT].
         */
        fun fromProperty(property: LogseqProperty): List<LogseqLink> {
            return buildList {
                property.values.forEach {
                    if (isLink(it)) {
                        add(LogseqLink(it))
                    } else if (property.name == LogseqProperty.TAGS) {
                        add(LogseqLink(it, true))
                    }
                }
            }
        }
    }
}

fun obsideTagName(name: String): String {

    var new = name.trim().replace(REMOVE_ON_OBSIDIAN_TAGS, "_")

    val first = new.firstOrNull()
    if (first == null) {
        throw IllegalArgumentException("logseq tag '$name' obsides to nothing")
    }

    if (first.isDigit()) {
        new = "d" + new
    }

    return new

}


private fun extractRawLinks(line: String): List<String> {
    val list = mutableListOf<String>()
    var i = 0

    while (i < line.length) {
        when (line[i]) {
            '#' -> {
                val tag = extractTagFromBeginOfLine(line.substring(i))
                if (tag != null) {
                    list.add(tag)
                    i += tag.length
                } else i++
            }

            '[' -> {
                val match = REGEX_WIKI_LINK_AT_LINE_START.find(line.substring(i))
                if (match != null) {
                    list.add(match.value)
                    i += match.value.length
                } else i++
            }

            else -> i++
        }
    }
    return list.toList()
}


/**
 * Call this method with a substring staring with '#'
 */
private fun extractTagFromBeginOfLine(lineFragment: String): String? {
    require(lineFragment[0] == '#') { "fragment has to start with #: $lineFragment" }
    if (lineFragment.length == 1) { // the # was the last char in line
        return null
    }

    val substring = lineFragment.substring(1)

    // check for #[[]]
    val match = REGEX_WIKI_LINK_AT_LINE_START.find(substring)
    if (match != null) {
        return "#${match.value}"
    }

    // check for plain tag (which can be anything in logseq)
    return when (substring[0]) {
        ' ', '#' -> null
        else -> lineFragment.substringBefore(" ")
    }
}

internal fun extractNameFromWikiLink(raw: String, checkStart: Boolean = true, checkTrimable: Boolean = false): String {
    if (checkStart) {
        require(raw.startsWith("[[")) { "wikilink must start with [[: '$raw'" }
    }
    require(raw.endsWith("]]")) { "wikilink must end with ]]: '$raw'" }
    val name = raw.drop(2).dropLast(2)
    require(name.isNotBlank()) { "wikilink cannot have blank name: $raw" }

    if (checkTrimable) {
        // not enforced by neither logseq nor obsidian
        require(name == name.trim()) { "wikilink should not be trimable: '$raw'" }
    }

    return name
}

/**
 * Gets the name from [LogseqLinkType.TAG] or [LogseqLinkType.MIXED]
 *
 * @throws IllegalArgumentException if [rawTag] is not a correct link
 */
internal fun extractNameFromTag(rawTag: String): String {
    require(rawTag.isNotBlank())
    require(rawTag.startsWith('#')) { "raw tag doesn't start with #: '$rawTag'" }

    val unhashed = rawTag.substring(1)

    if (unhashed.startsWith("[[")) {
        return extractNameFromWikiLink(unhashed, false)
    } else {
        require(unhashed == unhashed.trim()) { "pure tag cannot be trimable: $rawTag" }
        require(unhashed.isNotBlank()) { "tag cannot be blank: '$rawTag'" }
        require(!unhashed.startsWith("[")) { "pute tag cannot start with: '$rawTag'" }
        require(!unhashed.contains(REGEX_WHITES)) { "pure tag can't contain white spaces: '$rawTag'" }
        return unhashed.substringBefore(" ")
    }
}

