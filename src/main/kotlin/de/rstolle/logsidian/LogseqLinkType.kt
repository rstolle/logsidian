package de.rstolle.logsidian

/**
 *  * - `[[name]]` - normal link
 *  * - `#name` - a tag, which is a link as well in Logseq
 *  * - `#[[name with spaces]]` - a tag
 *
 */
enum class LogseqLinkType {
    WIKI {
        /**
         * Gets the name (target) of a link like `[[Peace]]`
         * @throws IllegalArgumentException if syntax is wrong
         */
        override fun extractName(raw: String): String {
            return extractNameFromWikiLink(raw)
        }
    },
    TAG {  // #peace -> could be also #1322 in opposite to obsidian
        override fun extractName(raw: String): String {
            require(!raw.startsWith("#[[")) { "expect a pure tag: '$raw'" }
            // everything else will be checked
            return extractNameFromTag(raw)
        }
    },
    MIXED {  // #[[Peace all over]] and of course [[Peace]]
        override fun extractName(raw: String): String {
            require(raw.startsWith("#[[")) { "tag link must start with #[[: $raw" }
            return WIKI.extractName(raw.substring(1))
        }
    },
    NONE {
        override fun extractName(raw: String): String {
            throw IllegalStateException("NONE is not a valid link type")
        }
    },
    EXPLICIT {
        override fun extractName(raw: String): String {
            return raw
        }
    };

    abstract fun extractName(raw: String): String

    fun isValid(raw: String): Boolean {
        try {
            extractName(raw)
        } catch (fail: IllegalArgumentException) {
            return false
        }
        return true
    }


    companion object {
        /**
         * Detects the type of the Logseq link based on the raw input string.
         *
         * CONSIDER: null instead of NONE ?
         *
         * @param raw The raw string representation of the link to be analyzed.
         * @return The detected [LogseqLinkType] for the input string, or [LogseqLinkType.NONE] if no valid type is found.
         */

        fun detect(raw: String): LogseqLinkType {
            entries.forEach {
                if (it != NONE && it != EXPLICIT && it.isValid(raw)) {
                    return it
                }
            }
            return NONE
        }
    }
}