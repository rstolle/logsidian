package de.rstolle.tools

import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.isDirectory
import kotlin.io.path.isReadable
import kotlin.io.path.isRegularFile


fun Path.requireReadableFolder(folderDescription: String = ""): Path {

    val description = if (folderDescription.isBlank())
        "Folder"
    else
        folderDescription

    require(exists()) { "$description does not exit: $this" }
    require(isReadable()) { "$description is not readable: $this " }
    require(isDirectory()) { "$description is not a directory: $this " }
    return this
}

fun Path.requireReadableFile(fileDescription: String = ""): Path {

    val description = if (fileDescription.isBlank())
        "File"
    else
        fileDescription

    require(exists()) { "$description does not exit: $this" }
    require(isReadable()) { "$description is not readable: $this " }
    require(isRegularFile()) { "$description is not a regular file: $this " }
    return this
}
