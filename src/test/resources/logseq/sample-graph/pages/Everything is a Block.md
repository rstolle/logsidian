- some to level content and now ... enter
- some more and now ... shift enter
  got a new line in the same block
  
  and one more after an empty line
  now I'll hit enter three times and then create a heading
-
-
- ## Heading 2
  id:: 65cd1fd3-fb16-4476-a584-0a079eeeb8d8
- Even a heading is a block on its own with a hyphen in beforehand.