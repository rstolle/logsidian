tags:: develop, #jvm, #native, #[[multi platform]]
web:: https://kotlinlang.org
wiki:: https://en.wikipedia.org/wiki/Kotlin_(programming_language)
alias:: Kotlin Programming Language, kt

-