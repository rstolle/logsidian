package de.rstolle.logsidian

import net.kafujo.io.Resources
import java.nio.file.Path

val SAMPLE_GRAPH: Path = Resources.ofCurrentThread().asPath("logseq/sample-graph")

private var wtf = listOf("wtf")

private var wtflol = listOf("wtf lol")
private var wtf_wtf = listOf("wtf", "wtf")
private var wtf_lol = listOf("wtf", "lol")

enum class LinesOK(val content: String, val tags: List<String>) {

    // no tag at all
    EMPTY("", emptyList()),
    NO_TAG("wtf", emptyList()),
    JUST_HEADING("# wtf", emptyList()),
    JUST_RUBBISH("lorem ipsus # wtf lorem", emptyList()),

    // one tag
    ONE_TAG_ONLY("#wtf", wtf),
    ONE_AT_LINE_START("#wtf loreem ipsus", wtf),
    ONE_AT_LINE_START2("#[[wtf]] loreem ipsus", wtf),
    ONE_AT_LINE_END("#wtf loreem ipsus", wtf),
    ONE_AT_LINE_END2("#[[wtf]] loreem ipsus", wtf),
    ONE_IN_LINE("loreem ipsus #wtf loreem ipsus", wtf),
    ONE_IN_LINE2("loreem ipsus #[[wtf]] loreem ipsus", wtf),
    ONE_WITH_SPACE("loreem ipsus #[[wtf lol]] loreem ipsus", wtflol),

    // two tags
    TWO_IN_LINE("loreem ipsus #wtf loreem #lol ipsus", wtf_lol),
    TWO_START_END("#wtf loreem ipsus #lol", wtf_lol),
    TWO_TIMES_WTF("#wtf loreem ipsus #wtf yeah", wtf_wtf),
}

val KOTLIN_MD_HEADER_YAML = """---
tags:
  - jvm
  - native
  - multi_platform
links:
  - "[[develop]]"
web: https://kotlinlang.org
wiki: https://en.wikipedia.org/wiki/Kotlin_(programming_language)
aliases:
  - Kotlin Programming Language
  - kt
---
"""