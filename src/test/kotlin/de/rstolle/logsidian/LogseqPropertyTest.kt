package de.rstolle.logsidian

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class LogseqPropertyTest {

    @Test
    fun `check for head lines`() {
        assertTrue(LogseqProperty.isValidPropertyLine("alias:: wtf"))
        assertTrue(LogseqProperty.isValidPropertyLine("alias::    wtf"))
        assertTrue(LogseqProperty.isValidPropertyLine("t:: w"))

        assertTrue(LogseqProperty.isValidPropertyLine("alias:: wtf"))
        assertTrue(LogseqProperty.isValidPropertyLine("alias::    wtf"))
        assertTrue(LogseqProperty.isValidPropertyLine("t:: w"))

        assertFalse(LogseqProperty.isValidPropertyLine("alias"))
        assertFalse(LogseqProperty.isValidPropertyLine("alias::"))
        assertFalse(LogseqProperty.isValidPropertyLine("::fasw"))
        assertFalse(LogseqProperty.isValidPropertyLine("fa sw::"))
        assertFalse(LogseqProperty.isValidPropertyLine("WTF::"))
    }


    @Test
    fun `extract properties`() {
        val wtf = listOf("wtf")
        val wtflol = listOf("wtf lol")
        val wtf_lol = listOf("wtf", "lol")
        val wtf_lol_rofl = listOf("wtf", "lol", "rofl")

        assertEquals(
            LogseqProperty("alias", wtf), LogseqProperty.orNull("alias:: wtf")
        )
        assertEquals(
            LogseqProperty("alias", wtflol), LogseqProperty.orNull("alias:: wtf lol")
        )
        assertEquals(
            LogseqProperty("alias", wtf_lol), LogseqProperty.orNull("alias:: wtf, lol")
        )
        assertEquals(
            LogseqProperty("alias", wtf_lol), LogseqProperty.orNull("alias::    wtf,  lol")
        )
        assertEquals(
            LogseqProperty("alias", wtf_lol_rofl), LogseqProperty.orNull("alias::    wtf,  lol, rofl")
        )
    }

    @Test
    fun `extract properties 2`() {
        assertEquals(
            LogseqProperty("alias", listOf("Sehr schlecht")), LogseqProperty.orNull("alias:: Sehr schlecht")
        )
    }

    @Test
    fun play() {
        val grapgh = LogseqGraph("/opt/lg/sq")

        val aliases = mutableSetOf<String>()

        println("------------------------------------- all found aliases:")
        println(aliases.sorted().joinToString(", "))


        println("------------------------------------- links with alias")
        val linknamesUpper = buildList {
            grapgh.links().forEach {
                add(it.name.uppercase())
            }
        }
        val common = linknamesUpper.intersect(aliases)
        println(common.joinToString("; "))

        // alle _circle starten mit kreis_ und werden reine tags


        println(countDifferentTypes(grapgh.links(LogseqLineType.CONTENT)))


        println("-------------------------------------")
        val grep = grapgh.grep("Eilenburg")

        println("grep.size = ${grep.size}")

        grep.forEach {
            println("${it.key} \t ${it.value}")
        }
    }

    @Test
    fun play2() {
        val graph = LogseqGraph("/opt/lg/sq")

        graph.assets.forEach {
            val size = graph.grep(it.last().toString()).size

            if (size > 0) {
                println(" linked: $size times: $it")
            }
        }
    }

    @Test
    fun tag_props() {
        val graph = LogseqGraph("/opt/lg/sq")

        val list = buildList {
            graph.pages.mdfiles.forEach { page ->
                val tags = page.propertyOrNull(LogseqProperty.TAGS)
                if (tags != null) {
                    addAll(tags.values)
                }
            }
        }

        println("tags total: ${list.size}")

        list.forEach {
            if (LogseqLink.isLink(it)) {
                println(it)
            }
        }

//        val map = list.groupingBy { it }.eachCount()
//
//        val sortedList = map.toList().sortedBy { (_, value) -> value }
//
//        sortedList.forEach { println("${it.second}  -> ${it.first}") }
//
    }
}

fun countDifferentTypes(links: List<LogseqLink>): Map<LogseqLinkType, Int> {
    return links.groupingBy { it.type }.eachCount()
}
