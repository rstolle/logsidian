package de.rstolle.logsidian

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class LogseqLinkTest {

    @Test
    fun `valid raw links`() {
        assertTrue(LogseqLink.isLink("#lol"))
        assertTrue(LogseqLink.isLink("[[lol]]"))
        assertTrue(LogseqLink.isLink("[[lol rofl]]"))
        assertTrue(LogseqLink.isLink("#[[lol rofl]]"))

        assertFalse(LogseqLink.isLink("lol sdf"))
        assertFalse(LogseqLink.isLink("#lol sdf"))
    }

    @Test
    fun `extract links from valid raw links`() {

        with(LogseqLink("[[wtf]]")) {
            println(this)
            assertEquals("wtf", name)
            assertEquals("[[wtf]]", raw)
            assertEquals(LogseqLinkType.WIKI, type)
        }

        with(LogseqLink("[[wtf lol]]")) {
            assertEquals("wtf lol", name)
            assertEquals("[[wtf lol]]", raw)
            assertEquals(LogseqLinkType.WIKI, type)
        }

        with(LogseqLink("#wtf")) {
            assertEquals("wtf", name)
            assertEquals("#wtf", raw)
            assertEquals(LogseqLinkType.TAG, type)
        }

        with(LogseqLink("#[[wtf]]")) {
            assertEquals("wtf", name)
            assertEquals("#[[wtf]]", raw)
            assertEquals(LogseqLinkType.MIXED, type)
        }

        with(LogseqLink("#[[wtf sdf]]")) {
            assertEquals("wtf sdf", name)
            assertEquals("#[[wtf sdf]]", raw)
            assertEquals(LogseqLinkType.MIXED, type)
        }
    }

    @Test
    fun `extract Links from a text`() {

        val links = LogseqLink.fromText(
            """
                - This is [[heavy]]! #HEAVY ! 
                -
            [[look here]] to understand #[[wtf lol]] 
            
            """
        )

        assertEquals(4, links.size)

        assertEquals("heavy", links[0].name)
        assertEquals("[[heavy]]", links[0].raw)
        assertEquals(LogseqLinkType.WIKI, links[0].type)

        assertEquals("HEAVY", links[1].name)
        assertEquals("#HEAVY", links[1].raw)
        assertEquals(LogseqLinkType.TAG, links[1].type)

        assertEquals(links[0], links[1])  // only the name counts and case insensitiv!!

        assertEquals("look here", links[2].name)
        assertEquals("[[look here]]", links[2].raw)
        assertEquals(LogseqLinkType.WIKI, links[2].type)

        assertEquals("wtf lol", links[3].name)
        assertEquals("#[[wtf lol]]", links[3].raw)
        assertEquals(LogseqLinkType.MIXED, links[3].type)
    }


    @Test
    fun `passing in invalid raw links`() {
        failLinkConstructionWithMessage("")
        failLinkConstructionWithMessage("#")
        failLinkConstructionWithMessage("#  ")
        failLinkConstructionWithMessage("#[[]]")
        failLinkConstructionWithMessage("#wtf sdf")
        failLinkConstructionWithMessage("# wtf sdf")
        failLinkConstructionWithMessage("# wtf")
        failLinkConstructionWithMessage(" #wtf")
        failLinkConstructionWithMessage("#[[]")
        failLinkConstructionWithMessage("#[[]")
        failLinkConstructionWithMessage("#[]]")
        failLinkConstructionWithMessage("#[]")
    }

    @Test
    fun `from Properties non explicit`() {
        val tags = LogseqProperty("type", listOf("jvm"))
        val links = LogseqLink.fromProperty(tags)
        assertEquals(0, links.size)
    }

    @Test
    fun `from Properties non explicit mixed`() {
        val tags = LogseqProperty("type", listOf("jvm", "#lol", "[[wtf sd]]"))
        val links = LogseqLink.fromProperty(tags)
        assertEquals(2, links.size)

        assertEquals("lol", links[0].name)
        assertEquals("wtf sd", links[1].name)

    }

    @Test
    fun `from Properties explicit`() {
        val tags = LogseqProperty(LogseqProperty.TAGS, listOf("jvm"))
        val links = LogseqLink.fromProperty(tags)
        assertEquals(1, links.size)
        with(links.first()) {
            assertEquals(LogseqLinkType.EXPLICIT, type)
            assertEquals("jvm", name)
            assertEquals("jvm", raw)
        }

        // it works only with tags!!
        val notags = LogseqProperty("notags", listOf("jvm"))
        assertEquals(0, LogseqLink.fromProperty(notags).size)

    }

    @Test
    fun `from Properties explicit mixed`() {
        val tags = LogseqProperty(LogseqProperty.TAGS, listOf("jvm", "#lol", "[[wtf sd]]"))
        val links = LogseqLink.fromProperty(tags)
        assertEquals(3, links.size)
        with(links[0]) {
            assertEquals(LogseqLinkType.EXPLICIT, type)
            assertEquals("jvm", name)
            assertEquals("jvm", raw)
        }

        with(links[1]) {
            assertEquals(LogseqLinkType.TAG, type)
            assertEquals("lol", name)
            assertEquals("#lol", raw)
        }

        with(links[2]) {
            assertEquals(LogseqLinkType.WIKI, type)
            assertEquals("wtf sd", name)
            assertEquals("[[wtf sd]]", raw)
        }
    }

    @Test
    fun `obside tag names`() {
        assertEquals("hello", obsideTagName("hello"))
        assertEquals("hello", obsideTagName(" hello  "))
        assertEquals("hello", obsideTagName(" hello\n"))
        assertEquals("hello_world", obsideTagName("hello world"))
        assertEquals("hello__world_", obsideTagName("hello (world)"))
        assertEquals("hello_1984", obsideTagName("hello_1984"))
        assertEquals("hello_1984", obsideTagName("hello 1984"))
        assertEquals("hello_1984_", obsideTagName("hello 1984!"))

        assertEquals("d1984", obsideTagName("1984"))
        assertEquals("d1984_possible_", obsideTagName("1984 possible?"))

        assertFails { obsideTagName("") }
        assertFails { obsideTagName(EMPTY_LINE) }
        assertFails { obsideTagName("   ") }
    }
}

private fun failLinkConstructionWithMessage(rawTag: String) {
    val t = assertFails(" *** Not a valid raw tag: '$rawTag' ***")
    {
        LogseqLink(rawTag)
    }
    tlgr.info("failed as expected: ${t.message}")
}
