package de.rstolle.logsidian

import de.rstolle.logsidian.LogseqLinkType.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class LogseqLinkTypeTest {

    @Test
    fun `validate link type TAG`() {
        assertTrue(TAG.isValid("#wtf"))

        assertFalse(TAG.isValid(""))
        assertFalse(TAG.isValid("#"))
        assertFalse(TAG.isValid("# "))
        assertFalse(TAG.isValid("[[wtf]]"))
        assertFalse(TAG.isValid("#wtf lol"))
        assertFalse(TAG.isValid("#[[sdfas]]"))
        assertFalse(TAG.isValid("#[wtf lol]"))
        assertFalse(TAG.isValid("#wtf lol"))
    }

    @Test
    fun `validate link type WIKI`() {
        assertTrue(WIKI.isValid("[[wtf]]"))
        assertTrue(WIKI.isValid("[[wtf lol]]"))

        assertFalse(WIKI.isValid(""))
        assertFalse(WIKI.isValid("#"))
        assertFalse(WIKI.isValid("# "))
        assertFalse(WIKI.isValid("#wtf lol"))
        assertFalse(WIKI.isValid("#[[sdfas]]"))
        assertFalse(WIKI.isValid("#[wtf lol]"))
        assertFalse(WIKI.isValid("#wtf lol"))
    }

    @Test
    fun `validate link type MIXED`() {
        assertTrue(MIXED.isValid("#[[wtf]]"))
        assertTrue(MIXED.isValid("#[[wtf lol]]"))

        assertFalse(MIXED.isValid(""))
        assertFalse(MIXED.isValid("#"))
        assertFalse(MIXED.isValid("# "))
        assertFalse(MIXED.isValid("#wtf lol"))
        assertFalse(MIXED.isValid("[[sdfas]]"))
        assertFalse(MIXED.isValid("#[wtf lol]"))
        assertFalse(MIXED.isValid("#wtf lol"))
    }


    @Test
    fun `detect link type NONE`() {
        assertEquals(NONE, LogseqLinkType.detect("[]"))
        assertEquals(NONE, LogseqLinkType.detect("#[]"))
        assertEquals(NONE, LogseqLinkType.detect("[[]]"))
        assertEquals(NONE, LogseqLinkType.detect("#[[]]"))
        assertEquals(NONE, LogseqLinkType.detect("  "))
        assertEquals(NONE, LogseqLinkType.detect(""))
        assertEquals(NONE, LogseqLinkType.detect("dafgjkldsa"))
    }

    @Test
    fun `detect link type`() {
        assertEquals(TAG, LogseqLinkType.detect("#tag"))
        assertEquals(WIKI, LogseqLinkType.detect("[[wtf]]"))
        assertEquals(WIKI, LogseqLinkType.detect("[[wtf lol]]"))
        assertEquals(MIXED, LogseqLinkType.detect("#[[Hello World]]"))
    }

    @Test
    fun `extract name from link type TAG`() {
        assertEquals("tag", TAG.extractName("#tag"))
        assertFails { TAG.extractName("#tag sdf") }
        assertFails { TAG.extractName("#[[tag sdf]]") }
    }

    @Test
    fun `extract name from link type WIKI`() {
        assertEquals("tag", WIKI.extractName("[[tag]]"))
        assertEquals("tag wtf", WIKI.extractName("[[tag wtf]]"))

        assertFails { WIKI.extractName("") }
        assertFails { WIKI.extractName("[[]]") }
        assertFails { WIKI.extractName("#tag sdf") }
        assertFails { WIKI.extractName("#[[tag sdf]]") }
    }

    @Test
    fun `extract name from link type MIXED`() {
        assertEquals("tag", MIXED.extractName("#[[tag]]"))
        assertEquals("tag wtf", MIXED.extractName("#[[tag wtf]]"))

        assertFails { MIXED.extractName("") }
        assertFails { MIXED.extractName("[[]]") }
        assertFails { MIXED.extractName("#tag sdf") }
    }


//    @Test
//    fun `parsing lines tag s`() {
//
//        for (line in LinesOK.entries) {
//            with(line) {
//                println(" *** test: ${line.content}")
//                val parsed = LogseqLink.extractLinkNames(content)
//                assertEquals(tags, parsed, "Test failed parsing line '$content'")
//            }
//        }
//    }
//
//    @Test
//    fun `parsing line`() {
//
//        val rawList = LogseqLink.extractLinkNames("#wtf loreem ipsus")
//        assertEquals("#wtf", rawList[0])
//
//        val parsed = LogseqLink.extractLinkNames("#[[wtf]] loreem ipsus")
//        assertEquals(listOf("wtf"), parsed)
//
//    }
}
