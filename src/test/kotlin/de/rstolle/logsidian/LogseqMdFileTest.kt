package de.rstolle.logsidian

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


class LogseqMdFileTest {

    @Test
    fun getPropertyNames() {
        val graph = LogseqGraph(SAMPLE_GRAPH)

        val page = graph.getPageOrNull("logsidian")
        assertNotNull(page)
        assertEquals("logsidian", page.name)
        assertTrue(page.containsProperty("tags"))

        assertEquals(1, page.properties.size)

        val tags = page.properties.first()
        assertEquals("tags", tags.name)
        assertEquals(2, tags.values.size)
        assertEquals("Kotlin", tags.values.first())
    }
}