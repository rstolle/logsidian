package de.rstolle.logsidian

import de.rstolle.logsidian.LogseqLineType.CONTENT
import de.rstolle.logsidian.LogseqLineType.PROPERTY
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue


class LogseqGraphTest {

    val graph = LogseqGraph(SAMPLE_GRAPH)

    @Test
    fun `print statistics`() {
        graph.printStats()
    }

    @Test
    fun `check folder names`() {
        with(graph) {
            assertTrue(pagesFolder.endsWith(DEFAULT_FOLDER_PAGES))
            assertTrue(journalsFolder.endsWith(DEFAULT_FOLDER_JOURNALS))
            assertNull(assetsFolder)
        }
    }

    @Test
    fun `Page Tags`() {
        assertEquals(8, graph.findPageTags(LogseqLinkType.NONE).size)
        assertEquals(4, graph.findPageTags(LogseqLinkType.EXPLICIT).size)
        assertEquals(1, graph.findPageTags(LogseqLinkType.WIKI).size)
        assertEquals(2, graph.findPageTags(LogseqLinkType.TAG).size)
        assertEquals(1, graph.findPageTags(LogseqLinkType.MIXED).size)
    }

    @Test
    fun `Page Tags pure`() {
        val allPageTags = graph.findPageTags(LogseqLinkType.NONE)

        assertEquals(8, allPageTags.size)
        assertEquals(4, allPageTags.filter { graph.isPureTag(it) }.size)

        val notPure = allPageTags.filter { !graph.isPureTag(it) }
        assertEquals(4, notPure.size) // 1x Kotlin 3x develop
        assertEquals(2, notPure.toSet().size) // Kotlin and develop
        assertEquals("develop", notPure.first().name)
        assertEquals(3, notPure.filter { it.name == "develop" }.size)
        assertEquals(1, notPure.filter { it.name == "Kotlin" }.size)

    }


    @Test
    fun `count functions`() {
        with(graph) {
            assertEquals(6, pages.mdfiles.size)
            assertEquals(1, journals.mdfiles.size)
            assertEquals(0, assets.size)

            val lines = lines()
            lines.forEach { println(" -> $it") }

            assertEquals(29, lines().count()) // TODO was 18 one commit before!?
            assertEquals(8, lines(PROPERTY).count())
            assertEquals(21, lines(CONTENT).count())

            assertEquals(11, links().size)
            assertEquals(8, links(PROPERTY).size)
            assertEquals(3, links(CONTENT).size)
        }
    }

    @Test
    fun `kotlin page header to yaml`() {
        val graph = LogseqGraph(SAMPLE_GRAPH)
        val page = graph.getPageOrNull("Kotlin")
        assertNotNull(page)

        assertEquals(KOTLIN_MD_HEADER_YAML, graph.obsideProperties(page))
    }


    @Test
    fun `obside some lines`() {
        val graph = LogseqGraph(SAMPLE_GRAPH)

        with(graph) {
            assertEquals(EMPTY_LINE, obsideLine(""))
            assertEquals(EMPTY_LINE, obsideLine(EMPTY_LINE))
            assertEquals(EMPTY_LINE, obsideLine("   "))
            assertEquals(EMPTY_LINE, obsideLine("       "))
            assertEquals(EMPTY_LINE, obsideLine("       \n"))


            assertEquals(" [[Kotlin]] is #native", obsideLine(" [[Kotlin]] is [[native]]"))
            assertEquals(
                " [[Kotlin]] and [[Java Programming Language]]",
                obsideLine(" #[[Kotlin]] and #[[Java Programming Language]]")
            )

            assertEquals(" #native vs #jvm", obsideLine(" [[native]] vs #jvm"))

            assertEquals(" [[Kotlin]] is #multi_platform", obsideLine(" #Kotlin is [[multi platform]]"))
            // todo: handle aliases smartly!
            assertEquals(" [[Kotlin]] vs #Java", obsideLine(" [[Kotlin]] vs [[Java]]"))
        }
    }
}
