package de.rstolle.logsidian

import org.junit.jupiter.api.Test
import kotlin.io.path.Path
import kotlin.io.path.fileSize
import kotlin.test.Ignore
import kotlin.test.assertNotNull

class LogseqLocalTest {

    @Ignore
    @Test
    fun `print all the non pure tags of a graph`() {
        val graph = LogseqGraph("/opt/lg/sq")

        val notPureTotal = graph.findPageTagsLinked()

        println("-----------------------------------------------------------")
        println("Not pure TOATL: ${notPureTotal.size}")
        println("-----------------------------------------------------------")

        val notPureNames = notPureTotal.map { it.name }.toSortedSet()
        notPureNames.forEachIndexed { idx, name ->
            println(
                "${idx + 1}  -> $name ->  ${notPureTotal.count { it.name == name }}  --> " +
                        "${graph.getPageOrNull(name)!!.path.fileSize()} bytes"
            )
        }
    }

    @Ignore
    @Test
    fun `header to yaml real`() {
        val graph = LogseqGraph("/opt/lg/sq")
//        val page = graph.getPageOrNull("XTAR VC4 Ladegerät")
        val page = graph.getPageOrNull("Ina")
        assertNotNull(page)

        val np = graph.obsideProperties(page)

        println(np)


    }

    @Ignore
    @Test
    fun `OBSIDE`() {
        val graph = LogseqGraph("/opt/tmp/logsidian/sq")

        graph.obside(Path("/opt/tmp/logsidian/t1"))

    }
}