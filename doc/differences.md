# Differences

Even though both apps work similar and have similar core features, there are some fundamental differences.
These will be covered here.

## file structure

Both apps operate on local files in a local folder. Obsidian maintains an arbitrary folder hierarchy where you can
place markdown and non-markdown files next to each other. There is a powerful file view wich can be used as a file
manager. You can structure your files by renaming, moving or using subfolders. This can even be done with
external tools and obsidian synchronizes immediately. Logseq is very different in this aspect:

- There is no file view - it's generally not possible to overlook the files _as files_ from within the app.
- There is **one** folder `pages` for all the markdown files. It's not possible to have subfolders.
- Next to `pages` in the logseq root folder lies the `journals` folder for daily notes, which are markdown as well.
- Logseq has one folder (`assets`) for additional files. It's not possible to structure these files with subfolders or
  by renaming. You can refer to these files by their names. But since there is no file view, you need to know the name
  somehow. This becomes even more inconvenient since logseq adds random characters to the file name on insert.

## Markdown syntax

Another key distinction between the two tools is there very basic layout of the markdown files. While obsidian sticks
close to vanilla markdown, logseq modifies the markdown slightly to archive its
[outline functionality](https://discuss.logseq.com/t/lesson-2-why-you-should-outline-and-link-your-notes/10038). It uses
hyphens to mark "blocks" (which can be referred to). And since everything is a block, almost every line starts with a
hyphen. This happens automatically while you write, everytime you hit enter you get a new bullet, which is a hyphen in
the markdown code. This is interpreted as a new block.

If you don't want a new block with each new line, use shift-Enter or switch to the "document mode" by typing `t d`.
Then you do "long form writing". But nonetheless you're within a block, now just a long one. There is no way around
those blocks and the leading hyphens. This interferes with the original syntax of the hyphen and creates
special markdown files not any markdown app can properly deal with. For example, this is the markdown for the first
version of [Everything is a Block.md](../src/test/resources/logseq/sample-graph/pages/Everything%20is%20a%20Block.md):

```markdown
- some to level content and now ... enter
- some more and now ... shift enter
  got a new line in the same block

  and one more after an empty line
  now I'll hit enter three times and then create a heading
-
-
- ## Heading 2
- Even a heading is a block on its own with a hyphen in beforehand.
```

As you can see, even the empty lines have a leading hyphen. This markdown was created with the logseq app and
displayed like this:

![Everything is a Block](img/Everything%20is%20a%20Block.png)

### Referring to logseq blocks

**Every** block can be referred to by using a UUID called 'block ref'. Right-click on the leading bullet to you get a
few options to deal with this. I just did a "copy block ref" on _Heading 2_ in
[Everything is a Block.md](../src/test/resources/logseq/sample-graph/pages/Everything%20is%20a%20Block.md)
and got this in the clipboard:

```
((65cd1fd3-fb16-4476-a584-0a079eeeb8d8))
```

When you do this the first time on a block, the UUID will also be stored in the markdown right underneath the headline:

```markdown
- ## Heading 2
  id:: 65cd1fd3-fb16-4476-a584-0a079eeeb8d8
- Even a heading is a block on its own with a hyphen in beforehand.
```

This is again something which breaks compatibility with other markdown viewers. I guess it's necessary to do to ensure
the UUID wont change anymore, when the markdown changes. 
